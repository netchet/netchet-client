package client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import javax.swing.JOptionPane;

public class Node {
	// The users IP ...
	private String ip;

	public Node() {
		try {
			// ... is obtained by creating a new socket and connecting to Cloudflare DNS servers and getting the IP used for it.
			Socket getIPSocket = new Socket();
			getIPSocket.connect(new InetSocketAddress("1.1.1.1", 80));
			ip = getIPSocket.getLocalAddress().toString().substring(1);
			getIPSocket.close();
		} catch (IOException e) {
			System.err.println("[ERR!]: This computer does not have an IPv4-address!");
			JOptionPane.showMessageDialog(NetChet.frame, "Make sure that your network connection is working!",
					"This computer does not have an IPv4-address", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}

	}

	public String getIP() {
		// Very simple getter.
		return ip;
	}
}