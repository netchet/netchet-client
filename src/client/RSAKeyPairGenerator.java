package client;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;

import javax.swing.JOptionPane;

public class RSAKeyPairGenerator {
	// This class generates a key pair
	
	private static PrivateKey privateKey;
	private static PublicKey publicKey;

	public static PrivateKey getPrivateKey() {
		return privateKey;
	}

	public static PublicKey getPublicKey() {
		return publicKey;
	}

	public static void init() {
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			keyGen.initialize(1024);
			KeyPair pair = keyGen.generateKeyPair();
			privateKey = pair.getPrivate();
			publicKey = pair.getPublic();

			// writeToFile("RSA/publicKey", getPublicKey().getEncoded());
			NetChet.encryptionPublicKey = Base64.getEncoder().encodeToString(getPublicKey().getEncoded());

			// writeToFile("RSA/privateKey", getPrivateKey().getEncoded());
			NetChet.encryptionPrivateKey = Base64.getEncoder().encodeToString(getPrivateKey().getEncoded());
		} catch (NoSuchAlgorithmException e) {
			System.out.println(
					"[ERR!]: RSA encryption error: Invalid algorithm!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA encryption error: Invalid algorithm!",
					"RSA encryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
}