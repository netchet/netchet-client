package client;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JOptionPane;

public class AES {
	/**
	 * Simple AES (Pre-shared key) encryption class.
	 * Supported operations are encrypt() and decrypt()
	 */
	
	private static SecretKeySpec secretKey;
	private static byte[] key;

	private static void setKey(String myKey) {
		MessageDigest sha;

		try {
			key = myKey.getBytes("UTF-8");
			sha = MessageDigest.getInstance("SHA-512");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16);
			secretKey = new SecretKeySpec(key, "AES");
		} catch (UnsupportedEncodingException e) {
			System.err.println("[ERR!]: The PSK-encryption is unsupported!");
			JOptionPane.showMessageDialog(NetChet.frame, "The PSK-Encryption is unsupported!", "PSK-encryption error",
					JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			System.err.println("[ERR!]: The PSK-Encryption could not find a matching hash algorithm!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"The PSK-Encryption could not find a matching hash algorithm!", "PSK-encryption error",
					JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	public static String encrypt(String strToEncrypt, String secret) {
		setKey(secret);

		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
		} catch (NoSuchAlgorithmException e) {
			System.err.println(
					"[ERR!]: The PSK-encryption could not find a matching encryption algorithm!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"The PSK-encryption could not find a matching encryption algorithm!",
					"PSK-encryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			System.err.println(
					"[ERR!]: The PSK-encryption could not create a matching padding!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"The PSK-encryption could not create a matching padding!",
					"PSK-encryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			System.err.println(
					"[ERR!]: The PSK-encryption could not find a valid key!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"The PSK-encryption could not find a valid key!",
					"PSK-encryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			System.err.println(
					"[ERR!]: The PSK-encryption encountered a wrong block size!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"The PSK-encryption encountered a wrong block size!",
					"PSK-encryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (BadPaddingException e) {
			System.err.println("[ERR!]: The PSK-encryption created a bad padding!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"The PSK-encryption created a bad padding!",
					"PSK-encryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			System.err.println("[ERR!]: The PSK-encryption is unsupported!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"The PSK-encryption is unsupported!", "PSK-encryption error",
					JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}

		return null;
	}

	public static String decrypt(String strToDecrypt, String secret) {
		setKey(secret);

		try {
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
		} catch (NoSuchAlgorithmException e) {
			System.err.println(
					"[ERR!]: The PSK-decryption could not find a matching encryption algorithm!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"The PSK-decryption could not find a matching encryption algorithm!",
					"PSK-decryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			System.err.println(
					"[ERR!]: The PSK-decryption could not create a matching padding!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"The PSK-decryption could not create a matching padding!",
					"PSK-decryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			System.err.println(
					"[ERR!]: The PSK-decryption could not find a valid key!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"The PSK-decryption could not find a valid key!",
					"PSK-decryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			System.err.println(
					"[ERR!]: The PSK-decryption encountered a wrong block size!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"The PSK-encryption encountered a wrong block size!",
					"PSK-decryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (BadPaddingException e) {
			System.err.println("[ERR!]: The PSK-decryption created a bad padding!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"The PSK-decryption created a bad padding!",
					"PSK-decryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}

		return null;
	}
}