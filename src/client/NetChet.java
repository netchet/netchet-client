package client;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.Timer;

public class NetChet {
	private static String brandName = "NetChet Client";
	private static String brandVersion = "V2.0";
	private static String brandNameVersion = brandName + " " + brandVersion;

	private static Node n;

	private static Socket clientSocket;

	private static String myIP = "";
	private static String peerIP = "";
	private static String serverIP = "";

	static JFrame frame;
	private static JLabel frameChatLabel;
	private static JTextField frameChatField;

	private static JButton frameChatSendButton;

	private static JButton frameSettingsButton;

	private static JDialog frameAddressSelect;
	private static JLabel frameAddressSelectAddressLabel;

	private static JLabel frameAddressSelectInfoPeerLabel;
	private static JTextField frameAddressSelectAddressPeerTextField;

	private static JLabel frameAddressSelectInfoServerLabel;
	private static JTextField frameAddressSelectAddressServerTextField;

	private static JButton frameAddressSelectButton;

	private static boolean receiveMessagesError = false;

	private static JFrame frameSettings;

	private static JLabel frameSettingsEncryptionLabel;

	private static ButtonGroup frameSettingsEncryptionButtonGroup;
	private static JRadioButton frameSettingsEncryptionNoneButton;
	private static JRadioButton frameSettingsEncryptionPSKButton;
	private static JRadioButton frameSettingsEncryptionPublicButton;

	private static JTextField frameSettingsEncryptionPSKKeyTextField;

	private static JButton frameSettingsOKButton;
	private static JButton frameSettingsCancelButton;

	private static String encryption = "None";

	private static String encryptionPSKKey = "";

	public static String encryptionPublicKey = "";
	public static String encryptionPrivateKey = "";

	private static String encryptionPeerPublicKey = "";

	private static JCheckBox frameSettingsDarkModeCheckBox;
	private static int darkModeI = 0;
	private static Timer darkModeTimer;

	public static void main(String[] args) {
		System.out.println("[INFO]: Starting " + brandNameVersion + " ...");
		
		// Obtain IP address
		n = new Node();
		myIP = n.getIP();
		System.out.println("[INFO]: Your IP is " + myIP);

		// Create main JFrame but don't show it yet
		frame = new JFrame(brandNameVersion);
		frame.setSize(1280, 720);
		frame.setLayout(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create address selection JFrame
		frameAddressSelect = new JDialog(frame, brandNameVersion, true);
		frameAddressSelect.setSize(512, 272);
		frameAddressSelect.setLayout(null);
		frameAddressSelect.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		// Show own address on address selection JFrame
		frameAddressSelectAddressLabel = new JLabel("Your IP: " + myIP);
		frameAddressSelectAddressLabel.setBounds(10, 0, 480, 40);
		frameAddressSelect.add(frameAddressSelectAddressLabel);

		// Show label that tells user to type the address of the partner
		frameAddressSelectInfoPeerLabel = new JLabel("Please type in the IPv4 address of your chat partner");
		frameAddressSelectInfoPeerLabel.setBounds(10, 20, 480, 40);
		frameAddressSelect.add(frameAddressSelectInfoPeerLabel);

		// Allows the user to type the partner's address
		frameAddressSelectAddressPeerTextField = new JTextField("");
		frameAddressSelectAddressPeerTextField.setBounds(10, 60, 480, 40);

		frameAddressSelectAddressPeerTextField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// When enter is pressed in the address text field, set the addresses to the correct values and start the chat
				peerIP = frameAddressSelectAddressPeerTextField.getText();
				serverIP = frameAddressSelectAddressServerTextField.getText();
				frameAddressSelect.setVisible(false);
				start();
			}
		});

		frameAddressSelect.add(frameAddressSelectAddressPeerTextField);

		// Show label that tells user to type the address of the relay server
		frameAddressSelectInfoServerLabel = new JLabel("Please type in the IPv4 address of the relay server");
		frameAddressSelectInfoServerLabel.setBounds(10, 100, 480, 40);
		frameAddressSelect.add(frameAddressSelectInfoServerLabel);

		// Allows the user to type the relay server's address
		frameAddressSelectAddressServerTextField = new JTextField("");
		frameAddressSelectAddressServerTextField.setBounds(10, 140, 480, 40);

		frameAddressSelectAddressServerTextField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// When enter is pressed in the address text field, set the addresses to the correct values and start the chat
				peerIP = frameAddressSelectAddressPeerTextField.getText();
				serverIP = frameAddressSelectAddressServerTextField.getText();
				frameAddressSelect.setVisible(false);
				start();
			}
		});

		frameAddressSelect.add(frameAddressSelectAddressServerTextField);

		// Button as an alternative to pressing Enter
		frameAddressSelectButton = new JButton("Go >");
		frameAddressSelectButton.setBounds(10, 190, 480, 40);

		frameAddressSelectButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Does the same thing as pressing Enter
				peerIP = frameAddressSelectAddressPeerTextField.getText();
				serverIP = frameAddressSelectAddressServerTextField.getText();
				frameAddressSelect.setVisible(false);
				start();
			}
		});

		frameAddressSelect.add(frameAddressSelectButton);

		frameAddressSelect.setVisible(true);
	}

	public static void start() {
		// Start the chat
		
		// This is the ChatLabel. It is used for showing incoming and outgoing messages.
		// We are using HTML inside the text of the label to make the output more colourful.
		frameChatLabel = new JLabel(
				"<html><FONT COLOR=BLACK>Chat with " + peerIP + " through " + serverIP + " started!</FONT><br>");
		frameChatLabel.setFont(new Font(frameChatLabel.getFont().getFamily(), Font.PLAIN, 28));
		frameChatLabel.setBounds(10, 10, 1240, 560);
		frameChatLabel.setOpaque(true);
		frameChatLabel.setBackground(new Color(255, 255, 255));
		frameChatLabel.setVerticalAlignment(JLabel.BOTTOM);
		frameChatLabel.setVerticalTextPosition(JLabel.BOTTOM);
		frame.add(frameChatLabel);

		// This is where the user types the message he/she wants to send.
		frameChatField = new JTextField();
		frameChatField.setBounds(10, 630, 1130, 40);

		frameChatField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// When enter is pressed, we try to send the message.
				trySendMessage(frameChatField.getText());
			}
		});

		frame.add(frameChatField);

		// This button is an alternative to pressing Enter
		frameChatSendButton = new JButton("Send >");
		frameChatSendButton.setBounds(1150, 630, 100, 40);

		frameChatSendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Does the same thing as pressing Enter
				trySendMessage(frameChatField.getText());
			}
		});

		frame.add(frameChatSendButton);

		// This initialises the settings JFrame but doesn't show it yet
		frameSettings = new JFrame(brandName + " Settings");
		frameSettings.setSize(1280, 720);
		frameSettings.setLayout(null);
		frameSettings.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

		// This shows the title encryption for ...
		frameSettingsEncryptionLabel = new JLabel("Encryption");
		frameSettingsEncryptionLabel.setBounds(10, 0, 480, 40);
		frameSettings.add(frameSettingsEncryptionLabel);

		frameSettingsEncryptionPSKKeyTextField = new JTextField();
		frameSettingsEncryptionPSKKeyTextField.setBounds(40, 70, 1200, 30);
		frameSettings.add(frameSettingsEncryptionPSKKeyTextField);

		// ... this button group.
		frameSettingsEncryptionButtonGroup = new ButtonGroup();

		// Button for disabling encryption
		frameSettingsEncryptionNoneButton = new JRadioButton("No encryption (default, not secure!)");
		frameSettingsEncryptionNoneButton.setBounds(20, 30, 1250, 20);

		frameSettingsEncryptionNoneButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// This disables the PSK key text field because no key is needed.
				frameSettingsEncryptionPSKKeyTextField.setEnabled(false);
			}
		});

		frameSettingsEncryptionButtonGroup.add(frameSettingsEncryptionNoneButton);
		frameSettings.add(frameSettingsEncryptionNoneButton);
		
		// Button for PSK encryption

		frameSettingsEncryptionPSKButton = new JRadioButton(
				"Pre-Shared-Key-encryption (key has to be shared through a different means of communication beforehand, secure)");
		frameSettingsEncryptionPSKButton.setBounds(20, 50, 1250, 20);

		frameSettingsEncryptionPSKButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// This enables the PSK key text field.
				frameSettingsEncryptionPSKKeyTextField.setEnabled(true);
			}
		});

		frameSettingsEncryptionButtonGroup.add(frameSettingsEncryptionPSKButton);
		frameSettings.add(frameSettingsEncryptionPSKButton);

		// Button for RSA encryption
		frameSettingsEncryptionPublicButton = new JRadioButton(
				"Public-Private-Key-encryption (no manual key exchange needed, secure)");
		frameSettingsEncryptionPublicButton.setBounds(20, 100, 1250, 20);

		frameSettingsEncryptionPublicButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// This disables the PSK text field because no key is needed.
				frameSettingsEncryptionPSKKeyTextField.setEnabled(false);
			}
		});

		frameSettingsEncryptionButtonGroup.add(frameSettingsEncryptionPublicButton);
		frameSettings.add(frameSettingsEncryptionPublicButton);

		
		// This is the check box for enabling the dark mode
		frameSettingsDarkModeCheckBox = new JCheckBox("Dark Mode");
		frameSettingsDarkModeCheckBox.setBounds(10, 140, 500, 20);

		frameSettingsDarkModeCheckBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				if (e.getStateChange() == 2) {
					// When the check box is enabled we loop from 0 to 238 in a timer for a nice animation to enable the dark mode
					
					darkModeI = 50;
					darkModeTimer = new Timer(10, new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							frame.getContentPane().setBackground(new Color(darkModeI, darkModeI, darkModeI));

							frameChatLabel.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameChatLabel.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameChatField.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameChatField.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameSettings.getContentPane().setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameSettings.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameSettingsEncryptionLabel.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameSettingsEncryptionLabel
							.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameSettingsEncryptionNoneButton.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameSettingsEncryptionNoneButton
							.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameSettingsEncryptionPSKButton.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameSettingsEncryptionPSKButton
							.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameSettingsEncryptionPSKKeyTextField
							.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameSettingsEncryptionPSKKeyTextField
							.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameSettingsEncryptionPublicButton
							.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameSettingsEncryptionPublicButton
							.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameSettingsDarkModeCheckBox.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameSettingsDarkModeCheckBox
							.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							darkModeI++;

							if (darkModeI == 238) {
								darkModeTimer.stop();
								darkModeI = 50;
							}
						}
					});

					darkModeTimer.start();
				}

				if (e.getStateChange() == 1) {
					// When the check box is enabled we loop from 238 to 0 in a timer for a nice animation to disable the dark mode

					
					darkModeI = 238;

					darkModeTimer = new Timer(10, new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							frame.getContentPane().setBackground(new Color(darkModeI, darkModeI, darkModeI));

							frameChatLabel.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameChatLabel.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameChatField.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameChatField.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameSettings.getContentPane().setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameSettings.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameSettingsEncryptionLabel.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameSettingsEncryptionLabel
							.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameSettingsEncryptionNoneButton.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameSettingsEncryptionNoneButton
							.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameSettingsEncryptionPSKButton.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameSettingsEncryptionPSKButton
							.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameSettingsEncryptionPSKKeyTextField
							.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameSettingsEncryptionPSKKeyTextField
							.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameSettingsEncryptionPublicButton
							.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameSettingsEncryptionPublicButton
							.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							frameSettingsDarkModeCheckBox.setBackground(new Color(darkModeI, darkModeI, darkModeI));
							frameSettingsDarkModeCheckBox
							.setForeground(new Color(255 - darkModeI, 255 - darkModeI, 255 - darkModeI));

							darkModeI--;

							if (darkModeI == 50) {
								darkModeTimer.stop();
								darkModeI = 238;
							}
						}
					});

					darkModeTimer.start();
				}
			}
		});

		frameSettings.add(frameSettingsDarkModeCheckBox);

		// This sets everything to the previous settings (the ones stored in variables).
		// This is also called when pressing the cancel button
		frameSettingsInit();

		// This is the OK button that sets the variables to the values that the user chose.
		frameSettingsOKButton = new JButton("OK");
		frameSettingsOKButton.setBounds(frameSettings.getWidth() - 100, frameSettings.getHeight() - 80, 70, 30);

		frameSettingsOKButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (frameSettingsEncryptionNoneButton.isSelected()) {
					frameSettings.setVisible(false);

					encryption = "None";

					System.out.println("[WARN]: Encryption was switched off or is still off!");

					frameSettingsInit();
				}

				else if (frameSettingsEncryptionPSKButton.isSelected()) {
					if (frameSettingsEncryptionPSKKeyTextField.getText().equals("")
							|| frameSettingsEncryptionPSKKeyTextField.getText() == "") {
						JOptionPane.showMessageDialog(frameSettings,
								"PSK-encryption was chosen but no Pre-Shared-Key was supplied!", "PSK-encryption error",
								JOptionPane.ERROR_MESSAGE);
					}

					else {
						frameSettings.setVisible(false);

						encryption = "PSK";
						encryptionPSKKey = frameSettingsEncryptionPSKKeyTextField.getText();

						System.out.println("[INFO]: PSK-encryption enabled");

						frameSettingsInit();
					}
				}

				else if (frameSettingsEncryptionPublicButton.isSelected()) {
					frameSettings.setVisible(false);

					encryption = "Public";

					RSAKeyPairGenerator.init();

					trySendMessage("-----BEGIN PUBLIC KEY-----" + encryptionPublicKey);

					System.out.println("[INFO]: Public-Private-Key-encryption enabled");

					frameSettingsInit();
				}
			}
		});

		frameSettings.add(frameSettingsOKButton);

		frameSettingsCancelButton = new JButton("Cancel");
		frameSettingsCancelButton.setBounds(frameSettings.getWidth() - 210, frameSettings.getHeight() - 80, 100, 30);

		frameSettingsCancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frameSettings.setVisible(false);

				// Here we call this again.
				frameSettingsInit();
			}
		});

		frameSettings.add(frameSettingsCancelButton);

		
		frameSettingsButton = new JButton("Settings");
		frameSettingsButton.setBounds(10, 580, 150, 40);

		frameSettingsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frameSettings.setVisible(true);
			}
		});

		frame.add(frameSettingsButton);

		frame.setVisible(true);

		// Start the connection
		connect();
	}

	public static void connect() {
		System.out.println("[INFO]: Connecting as client to " + peerIP + " through " + serverIP + " on port 33333 ...");

		try {
			// We only need to create a connection to the server, not to the chat partner.
			clientSocket = new Socket(serverIP, 33333);
			System.out.println("[INFO]: Connection to relay " + serverIP + " established");

			System.out.println("[INFO]: Connecting to " + peerIP + " ...");
			
			// This tries sending a message to the relay server that contains a request to chat with our partner
			trySendMessage("-----NETCHET CHATPAIR REQUEST-----" + peerIP);

			Runnable receiveMessagesRunnableClient = new Runnable() {
				// Now we keep repeating this:
				
				public void run() {
					while (true) {
						// Always try to receive a message if one comes in every second.
						
						tryreceiveMessage();
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							System.err.println("[ERR!]: The thread was interrupted while trying to receive a message!");
							JOptionPane.showMessageDialog(NetChet.frame,
									"The thread was interrupted while trying to receive a message!",
									"Error while receiving a message", JOptionPane.ERROR_MESSAGE);
							e.printStackTrace();
						}
					}
				}
			};

			new Thread(receiveMessagesRunnableClient).start();
		} catch (ConnectException e) {
			System.err.println("[ERR!]: Connection refused!");
			System.err
			.println("[ERR!]: Make sure that the server is running and that you supplied the correct address!");
			JOptionPane.showMessageDialog(frame,
					"Make sure that the server is running and that you supplied the correct address!",
					"Connection refused!", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (UnknownHostException e) {
			System.err.println("[ERR!]: Host not found!");
			System.err.println(
					"[ERR!]: Make sure that you supplied the correct address and that your network connection is working!");
			JOptionPane.showMessageDialog(frame,
					"Make sure that you supplied the correct address and that your network connection is working!",
					"Host not found", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("[ERR!]: Unknown error while connecting to server!");
			System.err.println(
					"[ERR!]: Make sure that you supplied the correct address and that your network connection is working!");
			JOptionPane.showMessageDialog(frame,
					"Make sure that you supplied the correct address and that your network connection is working!",
					"Unknown error while connecting to server", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	public static void trySendMessage(String m) {
		// This variable contains error code which is returned by sendMessage.
		int messageEvaluation = sendMessage(m);
		
		// It is then evaluated.
		
		// Everything is fine:
		if (messageEvaluation == 0) {
			// Show the message from the chat partner.
			frameChatLabel.setText(frameChatLabel.getText() + "<FONT COLOR=PURPLE>" + n.getIP()
			+ ": </FONT><FONT COLOR=BLUE>" + frameChatField.getText() + "</FONT><br>");
			frameChatField.setText("");
		}

		else if (messageEvaluation == 1) {
			// Deny sending empty messages.
			System.out.println("[WARN]: You cannot send empty messages!");
			JOptionPane.showMessageDialog(frame, "You cannot send empty messages!", "Warning",
					JOptionPane.WARNING_MESSAGE);
		}

		else if (messageEvaluation == 2) {
			// Show network error in case the network is not working.
			System.err.println("[ERR!]: Network error while sending message!");
			JOptionPane.showMessageDialog(frame, "Make sure that your network connection is working!",
					"Network error while sending message", JOptionPane.ERROR_MESSAGE);
		}

		else if (messageEvaluation == 3) {
			// Shows errors about encryption.
			System.err.println("[ERR!]: Public-Private-Key-encryption error while sending a message!");
			System.err.println("[ERR!]: Your chat partner is not using Public-Private-Key-encryption yet!");
			JOptionPane.showMessageDialog(frame, "Your chat partner is not using Public-Private-Key-encryption yet!",
					"Public-Private-Key-encryption error while sending a message", JOptionPane.ERROR_MESSAGE);
		}

		else {
			// Otherwise show unknown error.
			System.err.println("[ERR!]: An unknown error occured while sending a message!");
			JOptionPane.showMessageDialog(frame, "An unknown error occured while sending a message!", "Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public static int sendMessage(String m) {
		String message = m;

		// Is the message empty?
		if (message.equals("") || message.equals(null) || message == null) {
			return 1;
		}

		if ((message.contains("-----BEGIN PUBLIC KEY-----") == false)
				|| (message.contains("-----NETCHET CHATPAIR REQUEST-----") == false)) {
			// This stuff in here is only for real messages, not for public keys or chat pair requests.
			// If we have "unreal" messages like these, we don't encrypt them here.
			
			// No encryption:
			if (encryption.equals("None")) {
				// Nothing needs to be done here.
			}

			// PSK encryption:
			else if (encryption.equals("PSK")) {
				// Encrypts the message
				message = AES.encrypt(message, encryptionPSKKey);
			}

			// RSA encryption:
			else if (encryption.equals("Public")) {
				// If no public key from the chat partner (which we need to encrypt out message) was supplied yet, error.
				if (encryptionPeerPublicKey.equals("") || encryptionPeerPublicKey.equals(null)
						|| encryptionPeerPublicKey == null) {
					return 3;
				}
				
				// Otherwise encrypt the message.
				message = RSA.encrypt(message, encryptionPeerPublicKey);
			}
		}

		try {
			// After the messages have been encrypted or not we try to send them here.
			
			OutputStream outToServer = clientSocket.getOutputStream();
			DataOutputStream out = new DataOutputStream(outToServer);
			out.writeUTF(message);
			
			// Note that we do not need to print an error message for the catches here because we handle that in the function calling this one.
		} catch (IOException e) {
			e.printStackTrace();
			return 2;
		} catch (NullPointerException e) {
			e.printStackTrace();
			return 2;
		}

		// Everything was OK.
		return 0;
	}

	public static void tryreceiveMessage() {
		// This variable contains error code which is returned by sendMessage.
		int messageEvaluation = receiveMessage();

		// It is then evaluated.
		
		// Everything is fine:
		if (messageEvaluation == 0) {

		}

		// Network error:
		else if (messageEvaluation == 1) {
			if (receiveMessagesError == false) {
				System.err.println("[ERR!]: Network error while receiving a message!");
				JOptionPane.showMessageDialog(frame, "Make sure that your network connection is working!",
						"Network error while receiving a message", JOptionPane.ERROR_MESSAGE);
				receiveMessagesError = true;
			}
		}

		// Error message if the user is not using RSA encryption and his/her partner is:
		else if (messageEvaluation == 2) {
			System.err.println("[ERR!]: Public-Private-Key-encryption error while receiving a message!");
			System.err.println("[ERR!]: Your chat partner is using Public-Private-Key-encryption!");
			JOptionPane.showMessageDialog(frame, "Your chat partner is using Public-Private-Key-encryption!",
					"Public-Private-Key-encryption error while receiving a message", JOptionPane.ERROR_MESSAGE);
			receiveMessagesError = true;
		}

		// Unknown error:
		else {
			if (receiveMessagesError == false) {
				System.err.println("[ERR!]: An unknown error occurred while receiving a message!");
				JOptionPane.showMessageDialog(frame, "An unknown error occured while receiving a message!", "Error",
						JOptionPane.ERROR_MESSAGE);
				receiveMessagesError = true;
			}
		}
	}

	public static int receiveMessage() {
		try {
			// Try to get the message
			InputStream inFromServer = clientSocket.getInputStream();
			DataInputStream in = new DataInputStream(inFromServer);

			String message = in.readUTF();

			// Is it a public key?
			if (message.contains("-----BEGIN PUBLIC KEY-----")) {
				// --> no need for encryption, but cut unneeded string away to get public key
				encryptionPeerPublicKey = message.substring(26);

				if (encryption.equals("Public") == false) {
					// Here the partner has RSA encryption on but the user doesn't.
					return 2;
				}

				// Everything is fine.
				return 0;
			}

			else {
				// Here the normal messages are decrypted or not.
				
				// No encryption?
				if (encryption.equals("None")) {
					// No need to do anything.
				}

				// PSK encryption?
				else if (encryption.equals("PSK")) {
					// Decrypt using AES and the key that has been set.
					message = AES.decrypt(message, encryptionPSKKey);
				}

				// RSA encryption?
				else if (encryption.equals("Public")) {
					// Decrypt using RSA and the key given to the user automatically.
					message = RSA.decrypt(message, encryptionPrivateKey);
				}
			}

			// Add the plain text message to the output.
			frameChatLabel.setText(frameChatLabel.getText() + "<FONT COLOR=ORANGE>" + peerIP
					+ ": </FONT><FONT COLOR=RED>" + message + "</FONT><br>");
		} catch (IOException e) {
			if (receiveMessagesError == false) {
				e.printStackTrace();
				receiveMessagesError = true;
			}

			return 1;
		}

		return 0;
	}

	public static void frameSettingsInit() {
		// Sets the PSK key text field to the last set key
		frameSettingsEncryptionPSKKeyTextField.setText(encryptionPSKKey);

		// Resets all the buttons.
		if (encryption.equals("None")) {
			frameSettingsEncryptionNoneButton.setSelected(true);
			frameSettingsEncryptionPSKButton.setSelected(false);
			frameSettingsEncryptionPublicButton.setSelected(false);

			frameSettingsEncryptionPSKKeyTextField.setEnabled(false);
		}

		else if (encryption.equals("PSK")) {
			frameSettingsEncryptionNoneButton.setSelected(false);
			frameSettingsEncryptionPSKButton.setSelected(true);
			frameSettingsEncryptionPublicButton.setSelected(false);

			frameSettingsEncryptionPSKKeyTextField.setEnabled(true);
		}

		else if (encryption.equals("Public")) {
			frameSettingsEncryptionNoneButton.setSelected(false);
			frameSettingsEncryptionPSKButton.setSelected(false);
			frameSettingsEncryptionPublicButton.setSelected(true);

			frameSettingsEncryptionPSKKeyTextField.setEnabled(false);
		}
	}
}