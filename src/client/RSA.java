package client;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JOptionPane;

public class RSA {
	/**
	 * Simple RSA (Public private key) encryption class.
	 * Supported operations are setKey(), 
	 */
	
	private static PublicKey getPublicKey(String base64PublicKey) {
		PublicKey publicKey = null;

		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(base64PublicKey.getBytes()));

		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			publicKey = keyFactory.generatePublic(keySpec);
			return publicKey;
		} catch (NoSuchAlgorithmException e) {
			System.out.println("[ERR!]: RSA encryption error: Invalid hash algorithm!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA encryption error: Invalid hash algorithm!",
					"RSA encryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			System.out.println("[ERR!]: RSA encryption error: Invalid key spec!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA encryption error: Invalid key spec!",
					"RSA encryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}

		return publicKey;
	}

	public static String encrypt(String data, String publicKey) {
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
			return Base64.getEncoder().encodeToString(cipher.doFinal(data.getBytes()));
		} catch (NoSuchAlgorithmException e) {
			System.err.println("[ERR!]: RSA encryption error: Invalid algorithm!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA encryption error: Invalid algorithm!",
					"RSA encryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			System.err.println(
					"[ERR!]: RSA encryption error: No such padding!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA encryption error: No such padding!",
					"RSA encryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			System.err.println(
					"[ERR!]: RSA encryption error: Invalid key!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA encryption error: Invalid key!",
					"RSA encryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			System.err.println(
					"[ERR!]: RSA encryption error: Illegal block size!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA encryption error: Illegal block size!",
					"RSA encryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (BadPaddingException e) {
			System.err.println(
					"[ERR!]: RSA encryption error: Bad padding!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA encryption error: Bad padding!",
					"RSA encryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}

		return null;
	}

	private static PrivateKey getPrivateKey(String base64PrivateKey) {
		PrivateKey privateKey = null;

		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(base64PrivateKey.getBytes()));

		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			privateKey = keyFactory.generatePrivate(keySpec);
			return privateKey;
		} catch (NoSuchAlgorithmException e) {
			System.err.println("[ERR!]: RSA decryption error: No such algorithm!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA decryption error: No such algorithm!",
					"RSA decryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			System.err.println("[ERR!]: RSA decryption error: Invalid key spec!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA decryption error: Invalid key spec!",
					"RSA decryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}

		return privateKey;
	}

	public static String decrypt(String data, String privateKey) {
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.DECRYPT_MODE, getPrivateKey(privateKey));
			return new String(cipher.doFinal(Base64.getDecoder().decode(data.getBytes())));
		} catch (NoSuchAlgorithmException e) {
			System.err.println("[ERR!]: RSA decryption error: No such algorithm!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA decryption error: No such algorithm!",
					"RSA decryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			System.err.println(
					"[ERR!]: RSA decryption error: No such padding!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA decryption error: No such padding!",
					"RSA decryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			System.err.println(
					"[ERR!]: RSA decryption error: Invalid key!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA decryption error: Invalid key!",
					"RSA decryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			System.err.println(
					"[ERR!]: RSA decryption error: Illegal block size!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA decryption error: Illegal block size!",
					"RSA decryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (BadPaddingException e) {
			System.err.println(
					"[ERR!]: RSA decryption error: Bad padding!");
			JOptionPane.showMessageDialog(NetChet.frame,
					"RSA decryption error: Bad padding!",
					"RSA decryption error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}

		return null;
	}
}